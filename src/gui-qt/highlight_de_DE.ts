<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_US">
<context>
    <name>MainWindow</name>
    <message>
        <source>Themes or indent schemes not found.
Check installation.</source>
        <translation type="obsolete">Farb- oder Formatierungsvorlagen nicht gefunden.
Installation prüfen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="139"/>
        <location filename="mainwindow.cpp" line="159"/>
        <source>Initialization error</source>
        <translation>Initialisierungsfehler</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="140"/>
        <source>Could not read a colour theme: </source>
        <translation>Konnte dieses Farbschema nicht lesen: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="144"/>
        <source>light</source>
        <translation>Hell</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="145"/>
        <source>dark</source>
        <translation>Dunkel</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>B16 light</source>
        <translation>B16 Hell</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>B16 dark</source>
        <translation>B16 Dunkel</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="160"/>
        <source>Could not find syntax definitions. Check installation.</source>
        <translation>Syntaxdefinitionen nicht gefunden.
Installation prüfen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Always at your service</source>
        <translation>Stets zu Diensten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Select one or more files to open</source>
        <translation>Wähle eine oder mehrere Dateien zum Öffnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="314"/>
        <source>Select destination directory</source>
        <translation>Wähle das Zielverzeichnis</translation>
    </message>
    <message>
        <source>Tags file error</source>
        <translation type="obsolete">Tags Dateifehler</translation>
    </message>
    <message>
        <source>Could not read tags information in &quot;%1&quot;</source>
        <translation type="obsolete">Konnte Tags-Informationen in %1 nicht lesen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="971"/>
        <location filename="mainwindow.cpp" line="1002"/>
        <source>Output error</source>
        <translation>Ausgabefehler</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="971"/>
        <source>Output directory does not exist!</source>
        <translation>Zielverzeichnis existert nicht!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1002"/>
        <source>You must define a style output file!</source>
        <translation>Sie müssen ein Stylesheet zur Ausgabe angeben!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1051"/>
        <source>Processing %1 (%2/%3)</source>
        <translation>Verarbeite %1 (%2/%3)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1061"/>
        <source>Language definition error</source>
        <translation>Fehler in Sprachdefinition</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1062"/>
        <source>Invalid regular expression in %1:
%2</source>
        <translation>Ungültiger regulärer Ausdruck in %1: %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1066"/>
        <source>Unknown syntax</source>
        <translation>Unbekannte Syntax</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1066"/>
        <source>Could not convert %1</source>
        <translation>Konnte %1 nicht konvertieren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1069"/>
        <source>Lua error</source>
        <translation>Lua Fehler</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1069"/>
        <source>Could not convert %1:
Lua Syntax error: %2</source>
        <translation>Konnte %1 nicht konvertieren:\nLua Syntaxfehler: %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1213"/>
        <source>Converted %1 files in %2 ms</source>
        <translation>%1 Dateien in %2 ms konvertiert</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1398"/>
        <source>Conversion of &quot;%1&quot; not possible.</source>
        <translation>Konnte &quot;%1&quot; nicht konvertieren.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1398"/>
        <location filename="mainwindow.cpp" line="1539"/>
        <location filename="mainwindow.cpp" line="1582"/>
        <source>clipboard data</source>
        <translation>Daten der Zwischenablage</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1469"/>
        <source>%1 options</source>
        <translation>%1-Optionen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1512"/>
        <location filename="mainwindow.cpp" line="1523"/>
        <source>(user script)</source>
        <translation>(Benutzerskript)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>Preview (%1):</source>
        <translation>Vorschau (%1):</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1541"/>
        <source>Current syntax: %1 %2</source>
        <translation>Aktuelle Syntax: %1 %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1542"/>
        <source>Current theme: %1 %2</source>
        <translation>Farbschema: %1 %2</translation>
    </message>
    <message>
        <source>Current syntax: %1</source>
        <translation type="vanished">Aktuelle Syntax: %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1582"/>
        <source>Preview of &quot;%1&quot; not possible.</source>
        <translation>Vorschau von &quot;%1&quot; nicht möglich.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1828"/>
        <source>Some plug-in effects may not be visible in the preview.</source>
        <translation>Einige Plug-In Effekte könnten in der Vorschau nicht dargestellt werden.</translation>
    </message>
    <message>
        <source>Choose a ctags file</source>
        <translation type="obsolete">Wählen Sie eine ctags Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1622"/>
        <location filename="mainwindow.cpp" line="1626"/>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="1635"/>
        <source>Choose a style include file</source>
        <translation>Wählen Sie ein Eingabe-Stylesheet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1640"/>
        <source>About providing translations</source>
        <translation>Übersetzungen bereitstellen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1641"/>
        <source>The GUI was developed using the Qt toolkit, and translations may be provided using the tools Qt Linguist and lrelease.
The highlight.ts file for Linguist resides in the src/gui-qt subdirectory.
The qm file generated by lrelease has to be saved in gui-files/l10n.

Please send a note to as (at) andre-simon (dot) de if you have issues during translating or if you have finished or updated a translation.</source>
        <translation>Diese GUI wurde mit dem Qt Toolkit entwickelt, daher werden Übersetzungsdateien mit den Werkzeugen Qt Linguist und lrelease erstellt.
Die highlight.ts Datei für Linguist befindet sich im src/gui-qt Unterverzeichnis.
Die von lrelease ausgegebene qm-Datei wird in gui-files/l10n gespeichert.

Bitte senden Sie eine Nachricht an as (at) andre-simon (dot) de, wenn Sie Probleme bei der Übersetzung haben oder wenn Sie eine Übersetzung fertiggestellt bzw. aktualisiert haben.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1699"/>
        <source>Select one or more plug-ins</source>
        <translation>Wähle ein Plug-in</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1710"/>
        <source>Select one or more syntax or theme scripts</source>
        <translation>Wähle ein oder mehrere Syntax- oder Themeskripte aus</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1770"/>
        <source>Choose a plug-in input file</source>
        <translation>Wähle eine Eingabedatei für Plug-Ins aus</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="mainwindow.ui" line="181"/>
        <source>Choose the source code files you want to convert.</source>
        <translation>Wähle  Sourcecode-Dateien zur Konvertierung aus.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="187"/>
        <source>Choose input files</source>
        <translation>Dateien wählen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <source>List of input files.</source>
        <translation>Liste der Eingabedateien.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <source>Remove the selected input files.</source>
        <translation>Lösche die markierten Dateien aus der Eingabeliste.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="240"/>
        <location filename="mainwindow.ui" line="583"/>
        <location filename="mainwindow.ui" line="719"/>
        <source>Clear selection</source>
        <translation>Auswahl entfernen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="247"/>
        <source>Remove all input files.</source>
        <translation>Lösche  die Eingabeliste.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <location filename="mainwindow.ui" line="593"/>
        <location filename="mainwindow.ui" line="729"/>
        <source>Clear all</source>
        <translation>Alle entfernen</translation>
    </message>
    <message>
        <source>Output destination</source>
        <translation type="obsolete">Zielverzeichnis</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Output directory</source>
        <translation>Zielverzeichnis</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>Select the output directory.</source>
        <translation>Wähle das Ausgabeverzeichnis.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="311"/>
        <location filename="mainwindow.ui" line="670"/>
        <location filename="mainwindow.ui" line="1334"/>
        <location filename="mainwindow.ui" line="1603"/>
        <location filename="mainwindow.ui" line="1711"/>
        <location filename="mainwindow.ui" line="2006"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <source>Save output in the input file directories.</source>
        <translation>Speichere Ausgabe in den Verzeichnissen der Eingabedateien.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="349"/>
        <source>Write to source directories</source>
        <translation>In Quellverzeichnisse schreiben</translation>
    </message>
    <message>
        <source>Output options</source>
        <translation type="obsolete">Ausgabeoptionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="789"/>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="809"/>
        <source>Output for&amp;mat:</source>
        <oldsource>Output format:</oldsource>
        <translation>Ausgabeformat:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="831"/>
        <source>Choose an output format.</source>
        <translation>Wähle ein Ausgabeformat.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="835"/>
        <source>HTML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="840"/>
        <source>XHTML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="845"/>
        <source>LaTeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="850"/>
        <source>TeX</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <source>RTF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="860"/>
        <source>ODT</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="865"/>
        <source>SVG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="875"/>
        <source>ANSI ESC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="880"/>
        <source>XTerm256 ESC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="885"/>
        <source>Truecolor ESC</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="904"/>
        <source>Add line numbers to the output.</source>
        <translation>Füge Zeilennummern zur Ausgabe hinzu.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="907"/>
        <source>Add line numbers</source>
        <translation>Zeilennummerierung</translation>
    </message>
    <message>
        <source>Set line numbering start</source>
        <translation type="vanished">Lege den Beginn der Nummerierung fest</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="946"/>
        <source>Select the line number width.</source>
        <translation>Wähle die Stellenanzahl der Nummerierung.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="967"/>
        <source>Fill leading space of line numbers with zeroes.</source>
        <translation>Fülle Zeilennummerierung mit Nullen auf.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="970"/>
        <source>Pad with zeroes</source>
        <translation>Zeige führende Nullen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="983"/>
        <source>Generate output without document header and footer.</source>
        <translation>Erzeuge Ausgabe ohne Dokumentenkopf und -fußteil.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="986"/>
        <source>Omit header and footer</source>
        <translation>Kopf- und Fußteil auslassen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="993"/>
        <source>Output any plug-in text injections even if document header and footer is omitted.</source>
        <translation>Gib Textausgaben der Plug-Ins aus auch wenn Kopf- und Fußteil der Dokumente weggelassen werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <source>Keep Plug-In injections</source>
        <translation>Behalte Plug-In Ausgaben</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1003"/>
        <source>Test if input data is not binary.
Removes Unicode BOM mark.</source>
        <translation>Stellt sicher, daß die Eingabe nicht binär ist.
Entfernt den Unicode BOM.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1007"/>
        <source>Validate input data</source>
        <translation>Prüfe Eingabedaten</translation>
    </message>
    <message>
        <source>Set the output file ancoding.</source>
        <translation type="vanished">Setzt das Encoding.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1844"/>
        <source>I&amp;mage width:</source>
        <translation>Bild&amp;breite:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1892"/>
        <source>Height:</source>
        <oldsource>Image height:</oldsource>
        <translation>Höhe:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2150"/>
        <source>Reformat and indent your code.
This feature is enabled for C, C++, C# and Java code.</source>
        <translation>Formatiere den Eingabecode.
Diese Funktion kann auf C, C++, C# und Java-Code angewandt werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2481"/>
        <source>&amp;Plug-Ins</source>
        <translation>&amp;Plug-Ins</translation>
    </message>
    <message>
        <source>Encoding:</source>
        <translation type="obsolete">Encoding:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1036"/>
        <source>Select or define the encoding.
The result has to match the input file encoding.</source>
        <translation>Wähle oder definiere das Ausgabe-Encoding.
Das Encoding muss mit den Eingabedateien übereinstimmen.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <source>ISO-8859-1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1052"/>
        <source>ISO-8859-2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1057"/>
        <source>ISO-8859-3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1062"/>
        <source>ISO-8859-4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1067"/>
        <source>ISO-8859-5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1072"/>
        <source>ISO-8859-6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1077"/>
        <source>ISO-8859-7</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1082"/>
        <source>ISO-8859-8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1087"/>
        <source>ISO-8859-9</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1092"/>
        <source>ISO-8859-10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1097"/>
        <source>ISO-8859-11</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1102"/>
        <source>ISO-8859-12</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1107"/>
        <source>ISO-8859-13</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1112"/>
        <source>ISO-8859-14</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1117"/>
        <source>ISO-8859-15</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1122"/>
        <source>UTF-8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1146"/>
        <source>Output specific</source>
        <translation>Format-Optionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1379"/>
        <source>Add an anchor to each line.</source>
        <translation>Füge Anker zu jeder Zeile hinzu.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1443"/>
        <source>Add HTML MIME Type when copying code to the clipboard</source>
        <translation>Füge HTML MIME-Type hinzu wenn Code in die Zwischenablage kopiert wird</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1446"/>
        <source>Copy with MIME type</source>
        <translation>Kopiere mit MIME-Type</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1484"/>
        <source>Adapt output for the Babel package</source>
        <translation>Passe die Ausgabe für das Babel-Paket an</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1494"/>
        <source>Adapt output for the Beamer package</source>
        <translation>Passe die Ausgabe für das Beamer-Paket an</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1497"/>
        <source>Add Beamer compatibility</source>
        <translation>Beamer Kompatibilität</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1748"/>
        <source>Set page color attribute to background color.</source>
        <translation>Setze die Hintergrundfarbe als Seitenfarbe.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2486"/>
        <source>&amp;File access trace (W32)</source>
        <translation></translation>
    </message>
    <message>
        <source>HTML options</source>
        <translation type="obsolete">HTML Optionen</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="obsolete">Stylesheets</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1195"/>
        <location filename="mainwindow.ui" line="1521"/>
        <location filename="mainwindow.ui" line="1629"/>
        <location filename="mainwindow.ui" line="1924"/>
        <source>Include the style information in each output file.</source>
        <translation>Füge die Stylesheets in jede Ausgabedatei ein.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1198"/>
        <location filename="mainwindow.ui" line="1927"/>
        <source>Embed style (CSS)</source>
        <translation>CSS einbetten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <source>Add CSS information to each tag (do not use CSS class definitions).</source>
        <translation>Füge Stylesheets in jeden Tag ein (benutze keine CSS Klassen).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1214"/>
        <source>Inline CSS</source>
        <translation></translation>
    </message>
    <message>
        <source>Style file:</source>
        <translation type="obsolete">Stylesheet-Datei:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1274"/>
        <location filename="mainwindow.ui" line="1657"/>
        <location filename="mainwindow.ui" line="1952"/>
        <source>Name of the referenced style file.</source>
        <translation>Name des  referenzierten Stylesheets.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1277"/>
        <location filename="mainwindow.ui" line="1955"/>
        <source>highlight.css</source>
        <translation></translation>
    </message>
    <message>
        <source>Style include file:</source>
        <translation type="obsolete">Eingabe-Stylesheet:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1318"/>
        <location filename="mainwindow.ui" line="1990"/>
        <source>Path of the CSS include file.</source>
        <translation>Pfad zur CSS-Datei, die ins Stylesheet eingefügt werden soll.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Select a CSS include file.</source>
        <translation>Wähle eine Stylesheet-Eingabedatei.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1228"/>
        <source>CSS class prefix:</source>
        <translation>CSS Präfix:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <source>Shows description of a selected script.</source>
        <translation>Zeigt die Beschreibung des markierten Skripts an.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1241"/>
        <source>Add a CSS class name prefix to avoid namespace clashes.</source>
        <translation>Definiere einen CSS-Klassennamen, um Kollisionen mit anderen Stylesheets zu vermeiden.</translation>
    </message>
    <message>
        <source>Tags and index files</source>
        <translation type="obsolete">Tags- und Indexdateien</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1416"/>
        <source>Generate an index file with hyperlinks to all outputted files.</source>
        <translation>Erzeuge eine Indexdatei mit Links zu allen Ausgabedateien.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1419"/>
        <source>Generate index file</source>
        <translation>Indexdatei erzeugen</translation>
    </message>
    <message>
        <source>Read a ctags file and add the included metainformation as tooltips.
See ctags.sf.net for details.</source>
        <translation type="obsolete">Lese eine ctags-Datei und füge die Metainformationen als Tooltips hinzu.
Siehe ctags.sf.net um mehr darüber zu erfahren.</translation>
    </message>
    <message>
        <source>Ctags file:</source>
        <translation type="obsolete">Ctags Datei:</translation>
    </message>
    <message>
        <source>Path of the ctags file.</source>
        <translation type="obsolete">Pfad der ctags-Datei.</translation>
    </message>
    <message>
        <source>Choose a ctags file.</source>
        <translation type="obsolete">Wähle eine ctags Datei.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1360"/>
        <source>Misc</source>
        <translation>Verschiedenes</translation>
    </message>
    <message>
        <source>Add an achor to each line.</source>
        <translation type="vanished">Füge Anker zu jeder Zeile hinzu.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1382"/>
        <source>Add line anchors</source>
        <translation>Anker einfügen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1389"/>
        <source>Add the filename as prefix to the anchors.</source>
        <translation>Füge Dateinamen als Präfix zum Anker hinzu.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1392"/>
        <source>Include file name in anchor</source>
        <translation>Dateiname im Anker</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1399"/>
        <source>Output the lines within an ordered list.</source>
        <translation>Gib die Zeilen als sortierte Liste aus.</translation>
    </message>
    <message>
        <source>Ordered list</source>
        <translation type="obsolete">Sortierte Liste</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1426"/>
        <source>Add &amp;lt;pre&amp;gt; tags to the output, if the flag &quot;No document header and footer&quot; is selected.</source>
        <translation>Schließe die Ausgabe in  &amp;lt;pre&amp;gt;-Tags ein, wenn die Option &quot;Kopf- und Fußteil auslassen&quot; gewählt ist.</translation>
    </message>
    <message>
        <source>Enclose in &lt;pre&gt;</source>
        <translation type="obsolete">Schließe in &amp;lt;pre&amp;gt; ein</translation>
    </message>
    <message>
        <source>LaTeX options</source>
        <translation type="obsolete">LaTeX Optionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1474"/>
        <source>Replace quotes by dq sequences.</source>
        <translation>Ersetze Anführungszeichen mit \dq-Sequenzen.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1477"/>
        <source>Escape quotes</source>
        <translation>Ersetze Anführungszeichen</translation>
    </message>
    <message>
        <source>Make output Babel compatible.</source>
        <translation type="vanished">Erzeuge Code, der mit Babel verträglich ist.</translation>
    </message>
    <message>
        <source>Babel compatibility</source>
        <translation type="obsolete">Babel Kompatibilität</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1504"/>
        <source>Replace default symbols (brackets, tilde) by nice redefinitions.</source>
        <translation>Ersetze Standard-Darstellung verschiedener Symbole (Klammern, Tilde) durch schönere Definitionen.</translation>
    </message>
    <message>
        <source>Pretty symbols</source>
        <translation type="obsolete">Schönere Symbole</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1524"/>
        <source>Embed style (defs)</source>
        <translation>Stylesheet einbetten</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1549"/>
        <source>Name of the referenced  style file.</source>
        <translation>Name des  referenzierten Stylesheets.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1552"/>
        <location filename="mainwindow.ui" line="1660"/>
        <source>highlight.sty</source>
        <translation></translation>
    </message>
    <message>
        <source>Style include:</source>
        <translation type="obsolete">Eingabe-Stylesheet:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1029"/>
        <source>Set encoding:</source>
        <translation>Encoding:</translation>
    </message>
    <message>
        <source>Read ctags file:</source>
        <translation type="obsolete">Lese ctags-Datei:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1402"/>
        <source>Output as ordered list</source>
        <translation>Sortierte Liste</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1429"/>
        <source>Enclose in pre tags</source>
        <translation>Schließe in pre-Tags  ein</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1487"/>
        <source>Add Babel compatibility</source>
        <translation>Babel Kompatibilität</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1507"/>
        <source>Add pretty symbols</source>
        <translation>Symbole anpassen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1255"/>
        <location filename="mainwindow.ui" line="1533"/>
        <location filename="mainwindow.ui" line="1641"/>
        <location filename="mainwindow.ui" line="1936"/>
        <source>Stylesheet file:</source>
        <translation>Stylesheet-Datei:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="274"/>
        <source>Output destination:</source>
        <translation>Zielverzeichnis:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="330"/>
        <source>Browse output directory</source>
        <translation>Öffne das Ausgabeverzeichnis</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="397"/>
        <source>Copy highlighted code of the selected file into the clipboard.</source>
        <translation>Kopiere den Inhalt der ausgewählten Datei mit Highlighting in die Zwischenablage.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <source>Paste code from clipboard and copy the output back in one step</source>
        <translation>Füge Text aus der Zwischenablage ein und kopiere die formatierte Ausgabe in einem Schritt wieder zurück</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="488"/>
        <source>Paste, Convert and Copy</source>
        <translation>Einfügen, konvertieren und kopieren</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <source>Only output the selected lines of the preview.</source>
        <translation>Gib nur die in der Vorschau markierten Zeilen aus.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="517"/>
        <source>Output selected lines only</source>
        <translation>Nur selektierte Zeilen ausgeben</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="614"/>
        <source>Shows description of a selected plug-in script.</source>
        <translation>Zeigt Beschreibung des ausgewählten Plug-In Skripts.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <source>Scripts</source>
        <translation>Skripte</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="690"/>
        <source>Choose a custom syntax or theme script.</source>
        <translation>Wähle ein eigenes Syntax- oder Themeskript.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="693"/>
        <source>Add custom script to pool</source>
        <translation>Füge Skript zur Sammlung hinzu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="707"/>
        <source>You can check one file of each type (syntax and theme) to override the default behaviour. If checked, the scripts will be watched for changes.</source>
        <oldsource>You can check one file of each type (syntax and theme) to override the default behaviour.</oldsource>
        <translation>Du kannst eine Datei jeden Typs wählen (Syntax bzw. Theme), um die Ausgabe anzupassen. Bei Aktivierung wird das Skript auf Änderungen überwacht.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="716"/>
        <source>Remove the selected scripts.</source>
        <translation>Entferne ausgewählte Skripte aus der Liste.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <source>Remove all scripts.</source>
        <translation>Entferne alle Skripte aus der Liste.</translation>
    </message>
    <message>
        <source>Shows decription of a selected script.</source>
        <translation type="vanished">Zeigt die Beschreibung des gewählten Skripts an.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Script description</source>
        <translation>Skriptbeschreibung</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="927"/>
        <source>Set line numbering start.</source>
        <translation>Lege den Beginn der Nummerierung fest.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1014"/>
        <source>Generate output without version information comment.</source>
        <translation>Erzeuge Ausgabe ohne Versionskommentar.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1017"/>
        <source>Omit version info comment</source>
        <translation>Versionskommentar auslassen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1026"/>
        <source>Set the output file encoding.</source>
        <translation>Das Ausgabe-Encoding bestimmen.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1288"/>
        <location filename="mainwindow.ui" line="1563"/>
        <location filename="mainwindow.ui" line="1671"/>
        <location filename="mainwindow.ui" line="1966"/>
        <source>Stylesheet include file:</source>
        <translation>Eingabe-Stylesheet:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1372"/>
        <source>Line numbering options:</source>
        <translation>Optionen der Zeilennummerierung:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <source>Highlight</source>
        <translation>Highlight</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="175"/>
        <source>Files</source>
        <translation>Dateien</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="436"/>
        <source>Paste clipboard content into the preview window.</source>
        <translation>Füge Inhalt der Zwischenablage in die Vorschau ein.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="439"/>
        <source>Paste from clipboard (%1)</source>
        <oldsource>Paste from clipboard</oldsource>
        <translation>Aus Zwischenablage einfügen (%1)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="456"/>
        <source>Copy highlighted code into the clipboard.</source>
        <translation>Kopiere formatierte Ausgabe in die Zwischenablage.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="459"/>
        <source>Copy preview to clipboard (%1)</source>
        <oldsource>Copy preview to clipboard</oldsource>
        <translation>In Zwischenablage kopieren (%1)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <source>Select syntax:</source>
        <translation>Syntax wählen:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="427"/>
        <source>Select the correct syntax of the code snippet.</source>
        <translation>Wähle die zum Text passende Syntax.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="542"/>
        <source>Plug-ins</source>
        <translation>Plug-Ins</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="551"/>
        <source>Add plug-in to pool</source>
        <translation>Füge Plug-In zur Sammlung hinzu</translation>
    </message>
    <message>
        <source>List of plug-ins.</source>
        <translation type="obsolete">Sammlung von Plug-Ins.</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Remove the selected plug-ins.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">Entferne die selektierten Plug-Ins</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Remove all plug-ins.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">Entferne alle Plug-Ins</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:8pt;&quot;&gt;Use the selected plugin.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">Bneutze das ausgewählte Plug-In</translation>
    </message>
    <message>
        <source>Enable selected plug-in script</source>
        <translation type="obsolete">Aktiviere das selektierte Plug-In Skript</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="870"/>
        <source>BBCode</source>
        <translation>BBCode</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1187"/>
        <source>Stylesheets</source>
        <translation>Stylesheets</translation>
    </message>
    <message>
        <source>Include:</source>
        <translation type="vanished">Eingabedatei:</translation>
    </message>
    <message>
        <source>Index/ctags</source>
        <translation type="obsolete">Index/ctags</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1587"/>
        <location filename="mainwindow.ui" line="1695"/>
        <source>Path of the style include file.</source>
        <translation>Pfad zur sty-Datei, die ins Stylesheet eingefügt werden soll.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1600"/>
        <location filename="mainwindow.ui" line="1708"/>
        <location filename="mainwindow.ui" line="2003"/>
        <source>Select a style include file.</source>
        <translation>Wähle eine Stylesheet-Eingabedatei.</translation>
    </message>
    <message>
        <source>TeX options</source>
        <translation type="obsolete">TeX Optionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1632"/>
        <source>Embed style (macros)</source>
        <translation>Stylesheet einbetten</translation>
    </message>
    <message>
        <source>RTF options</source>
        <translation type="obsolete">RTF Optionen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1737"/>
        <source>Add character stylesheets with formatting information.
You can select the stylesheets in your word processor to reformat additional text.</source>
        <translation>Füge Zeichenvorlagen mit Formatierungsinformationen hinzu.
Diese können in der Textverarbeitung gewählt werden, um zusätzlichen Text zu formatieren.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1741"/>
        <source>Add character styles</source>
        <translation>Zeichenvorlagen</translation>
    </message>
    <message>
        <source>Set page color attribute to background color</source>
        <translation type="vanished">Setze die Hintergrundfarbe als Seitenfarbe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1751"/>
        <source>Set page color</source>
        <translation>Seitenfarbe setzen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1760"/>
        <source>Page size:</source>
        <translation>Papierformat:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1773"/>
        <source>Select a page size.</source>
        <translation>Wähle eine Seitengröße aus.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1780"/>
        <source>A3</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1785"/>
        <source>A4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1790"/>
        <source>A5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1795"/>
        <source>B4</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1800"/>
        <source>B5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1805"/>
        <source>B6</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1810"/>
        <source>Letter</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1815"/>
        <source>Legal</source>
        <translation></translation>
    </message>
    <message>
        <source>SVG options</source>
        <translation type="obsolete">SVG Optionen</translation>
    </message>
    <message>
        <source>Image size:</source>
        <translation type="vanished">Bildgröße:</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation type="vanished">Breite:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1872"/>
        <source>Enter the SVG width (may contain units).</source>
        <translation>Gib die Bildbreite ein (kann Größeneinheiten enthalten).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1908"/>
        <source>Enter the SVG height (may contain units).</source>
        <translation>Gib die Bildhöhe ein (kann Größeneinheiten enthalten).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2032"/>
        <source>No options defined.</source>
        <translation>Keine Optionen vorhanden.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2057"/>
        <source>Formatting</source>
        <translation>Formatierung</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2086"/>
        <source>Color theme:</source>
        <translation>Farbschema:</translation>
    </message>
    <message>
        <source>Toggle classic theme or Base16 theme selection.</source>
        <translation type="vanished">Wechsle zwischen den klassischen und den Base16 Themes.</translation>
    </message>
    <message>
        <source>Base16</source>
        <translation type="vanished">Base16</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2133"/>
        <source>Select a colour theme.</source>
        <translation>Wähle ein Farbschema.</translation>
    </message>
    <message>
        <source>Reformat and indent your code.
This feature is enabled tor C, C++, C# and Java code.</source>
        <translation type="vanished">Formatiere den Eingabecode.
Diese Funktion kann auf C, C++, C# und Java-Code angewandt werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2154"/>
        <source>Reformat:</source>
        <translation>Formatieren:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2173"/>
        <source>Choose a formatting scheme.</source>
        <translation>Wähle ein Formatierungs-Schema.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2187"/>
        <source>Change the keyword case.</source>
        <translation>Ändere die Groß- und Kleinschreibung, wenn die Eingabesyntax nicht case-sensitive ist.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2190"/>
        <source>Keyword case:</source>
        <translation>Schlüsselwort Case:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2209"/>
        <source>Select a keyword case.</source>
        <translation>Wähle einen Typ der Groß- und Kleinschreibung.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2216"/>
        <source>UPPER</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2221"/>
        <source>lower</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2226"/>
        <source>Capitalize</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2244"/>
        <source>Tab width:</source>
        <translation>Tabulatorbreite:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2260"/>
        <source>Enter the number of spaces which replace a tab.
Set the width to 0 to keep tabs.</source>
        <translation>Gib die Anzahl der Leerzeichen an, die ein Tab ersetzen.
Setze die Anzahl auf Null, um Tabs auszugeben.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2275"/>
        <source>Enable line wrapping.</source>
        <translation>Aktiviere den automatischen Zeilenumbruch.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2278"/>
        <source>Line wrapping</source>
        <translation>Zeilenumbruch</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2291"/>
        <source>Enter the maximum line length.</source>
        <translation>Gib die maximale Zeilenlänge ein.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2310"/>
        <source>Indent statements and function parameters after wrapping.</source>
        <translation>Rücke Kommandos und Funktionsparameter nach dem Umbruch ein.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2313"/>
        <source>Intelligent wrapping</source>
        <translation>Intelligenter Umbruch</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2334"/>
        <source>Font na&amp;me:</source>
        <oldsource>Font name:</oldsource>
        <translation>Schriftart:</translation>
    </message>
    <message>
        <source>Select or enter the font name.</source>
        <translation type="obsolete">Wähle oder gib die Schriftart an.</translation>
    </message>
    <message>
        <source>Font size:</source>
        <translation type="vanished">Schriftgröße:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2392"/>
        <source>Enter the font size.</source>
        <translation>Gib die Schriftgröße ein.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2465"/>
        <source>&amp;Visit andre-simon.de</source>
        <oldsource>Visit andre-simon.de</oldsource>
        <translation>&amp;Besuche andre-simon.de</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2476"/>
        <source>&amp;Dock floating panels</source>
        <translation>&amp;Schwebende Fenster andocken</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
    <message>
        <source>Visit website</source>
        <translation type="obsolete">Website aufrufen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Start the conversion of your input files.</source>
        <translation>Starte die Konvertierung der Eingabedateien.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="387"/>
        <source>Convert files</source>
        <translation>Konvertieren</translation>
    </message>
    <message>
        <source>Copy highlighted code of the seleted file into the clipboard.</source>
        <translation type="vanished">Kopiere den Inhalt der ausgewählten Datei mit Highlighting in die Zwischenablage.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <source>Clipboard</source>
        <translation>Zwischenablage</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="400"/>
        <source>Copy file to clipboard</source>
        <translation>Datei in Zwischenablage</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="548"/>
        <source>Choose a plug-in script.</source>
        <translation>Wähle ein Plug-In Skript aus.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="562"/>
        <source>List of plug-ins. Toggle checkbox to enable the scripts. The preview window may not display all plug-in effects.</source>
        <oldsource>List of plug-ins. Toggle checkbox to enable the scripts.</oldsource>
        <translation>Liste der Plug-Ins. Klicke auf die Checkboxen um Skripte zu aktivieren. Die Vorschau zeigt möglicherweise nicht alle Effekte an.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="580"/>
        <source>Remove the selected plug-ins.</source>
        <translation>Entferne ausgewählte Plug-Ins aus der Liste.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="590"/>
        <source>Remove all plug-ins.</source>
        <translation>Entferne alle Plug-Ins.</translation>
    </message>
    <message>
        <source>Use the selected plugin.</source>
        <translation type="obsolete">Benutze das ausgewählte Plug-In.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Output progress:</source>
        <translation>Ausgabefortschritt:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="110"/>
        <source>Wi&amp;ndows</source>
        <oldsource>&amp;Windows</oldsource>
        <translation>&amp;Fenster</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="130"/>
        <source>H&amp;ighlighting options</source>
        <oldsource>Highlighting options</oldsource>
        <translation>Hervorhebungs-Optionen</translation>
    </message>
    <message>
        <source>You can apply system shortcuts for copy and paste.</source>
        <translation type="vanished">Sie können die System-Tastenkombinationen zum Kopieren und Einfügen verwenden.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="602"/>
        <location filename="mainwindow.ui" line="738"/>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Shows decription of a selected plug-in script.</source>
        <translation type="vanished">Zeigt Beschreibung des ausgewählten Plug-In Skripts.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="617"/>
        <source>Plug-In description</source>
        <translation>Plug-In Beschreibung</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="645"/>
        <source>Plug-in parameter</source>
        <oldsource>Plug-in input file</oldsource>
        <translation>Plug-In Parameter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="654"/>
        <source>Optional plug-in parameter, this may be a path to a plug-in input file</source>
        <oldsource>Optional path to a plug-in input file</oldsource>
        <translation>Optionaler Parameter, dies kann ein Pfad zu einer Eingabedatei sein</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="667"/>
        <source>Select the plug-in input file.</source>
        <translation>Wähle eine Plug-In Eingabedatei.</translation>
    </message>
    <message>
        <source>Replace spaces by nbsp entities.</source>
        <oldsource>Replace spaces by &amp;nbsp; entities.</oldsource>
        <translation type="obsolete">Ersetze Leerzeichen durch nbsp-Codes.</translation>
    </message>
    <message>
        <source>Use non breaking spaces</source>
        <translation type="obsolete">Benutze geschützte Leerzeichen</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2320"/>
        <source>Do not add line numbering to lines which were automatically wrapped.</source>
        <translation>Gib bei automatisch umgebrochenen Zeilen keine Nummerierung aus.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2323"/>
        <source>Omit line numbers of wrapped lines</source>
        <translation>Keine Nummerierung bei Umbruch</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2355"/>
        <source>Select or enter the font name. HTML supports a list of fonts, separated with comma.</source>
        <translation>Wähle oder gib den Fontnamen ein. HTML unterstützt auch eine mit Komma getrennte Liste von Fonts.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2370"/>
        <source>Font si&amp;ze:</source>
        <translation>Schrift&amp;größe:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2407"/>
        <source>&amp;Open files</source>
        <translation>Öffne &amp;Dateien</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2412"/>
        <source>&amp;Exit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2417"/>
        <source>&amp;Load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2422"/>
        <source>&amp;Save</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2427"/>
        <source>Load &amp;default project</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2432"/>
        <source>&amp;Readme</source>
        <oldsource>Readme</oldsource>
        <translation>&amp;Handbuch</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2437"/>
        <source>&amp;Tips</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2445"/>
        <source>&amp;Changelog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2450"/>
        <source>&amp;License</source>
        <translation>&amp;Lizenz</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2455"/>
        <source>&amp;About Highlight</source>
        <translation>&amp;Über Highlight</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2460"/>
        <source>A&amp;bout translations</source>
        <oldsource>About &amp;translations</oldsource>
        <translation>&amp;Infos zu Übersetzungen</translation>
    </message>
</context>
<context>
    <name>ShowTextFile</name>
    <message>
        <location filename="showtextfile.ui" line="17"/>
        <source>Show text</source>
        <translation>Textanzeige</translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="37"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="71"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>io_report</name>
    <message>
        <location filename="io_report.ui" line="14"/>
        <source>Error summary</source>
        <translation>Fehlerübersicht</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="24"/>
        <source>Input errors:</source>
        <translation>Eingabefehler:</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="34"/>
        <source>Remove files above from input file list</source>
        <translation>Obige Dateien aus der Eingabeliste löschen</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="48"/>
        <source>Output errors:</source>
        <translation>Ausgabefehler:</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="65"/>
        <source>Reformatting not possible:</source>
        <translation>Neuformatierung nicht möglich:</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="82"/>
        <source>Failed syntax tests:</source>
        <translation>Fehlgeschlagene Syntaxtests:</translation>
    </message>
</context>
<context>
    <name>syntax_chooser</name>
    <message>
        <location filename="syntax_chooser.ui" line="14"/>
        <source>Syntax selection</source>
        <translation>Syntaxauswahl</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="32"/>
        <source>Select correct syntax</source>
        <translation>Syntax bestimmen</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="50"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="74"/>
        <source>These entries are configured in filetypes.conf.</source>
        <translation>Diese Einträge werden in der filetypes.conf festgelegt.</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="81"/>
        <source>The selection will be remembered for this session.</source>
        <translation>Die Auswahl wird für diese Sitzung gespeichert.</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="84"/>
        <source>Remember selection</source>
        <translation>Auswahl merken</translation>
    </message>
    <message>
        <location filename="syntax_chooser.cpp" line="57"/>
        <source>The file extension %1 is assigned to multiple syntax definitions.
Select the correct one:</source>
        <translation>Die Dateiendung %1 ist mehreren Sprachdefinitionen zugeordnet.
Wähle die richtige aus:</translation>
    </message>
</context>
</TS>
