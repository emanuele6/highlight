<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="139"/>
        <location filename="mainwindow.cpp" line="159"/>
        <source>Initialization error</source>
        <translation>Erreur lors de l&apos;initialisation</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="140"/>
        <source>Could not read a colour theme: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="144"/>
        <source>light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="145"/>
        <source>dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="146"/>
        <source>B16 light</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="147"/>
        <source>B16 dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="160"/>
        <source>Could not find syntax definitions. Check installation.</source>
        <translation>Impossible de trouver les définitions des syntaxes. Vérifiez votre installation.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="241"/>
        <source>Always at your service</source>
        <translation>Toujours à votre service</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="277"/>
        <source>Select one or more files to open</source>
        <translation>Sélectionnez un ou plusieurs fichiers à ouvrir</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="314"/>
        <source>Select destination directory</source>
        <translation>Sélectionnez un répertoire de destination</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="971"/>
        <location filename="mainwindow.cpp" line="1002"/>
        <source>Output error</source>
        <translation>Erreur de sortie</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="971"/>
        <source>Output directory does not exist!</source>
        <translation>Le répertoire de destination n&apos;existe pas !</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1002"/>
        <source>You must define a style output file!</source>
        <translation>Vous devez définir un fichier de style de sortie !</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1051"/>
        <source>Processing %1 (%2/%3)</source>
        <translation>Traitement de %1 (%2/%3)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1061"/>
        <source>Language definition error</source>
        <translation>Erreur de définition du langage</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1062"/>
        <source>Invalid regular expression in %1:
%2</source>
        <translation>Expression régulière non valide dans %1 :
%2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1066"/>
        <source>Unknown syntax</source>
        <translation>Syntaxe inconnue</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1066"/>
        <source>Could not convert %1</source>
        <translation>Impossible de convertir %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1069"/>
        <source>Lua error</source>
        <translation>Erreur Lua</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1069"/>
        <source>Could not convert %1:
Lua Syntax error: %2</source>
        <translation>Impossible de convertir %1 :
Erreur de syntaxe Lua : %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1213"/>
        <source>Converted %1 files in %2 ms</source>
        <translation>%1 fichier(s) converti(s) en %2 ms</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1398"/>
        <source>Conversion of &quot;%1&quot; not possible.</source>
        <translation>Conversion de &quot;%1&quot; impossible.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1398"/>
        <location filename="mainwindow.cpp" line="1539"/>
        <location filename="mainwindow.cpp" line="1582"/>
        <source>clipboard data</source>
        <translation>données du presse-papiers</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1469"/>
        <source>%1 options</source>
        <translation>Options de %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1512"/>
        <location filename="mainwindow.cpp" line="1523"/>
        <source>(user script)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>Preview (%1):</source>
        <translation>Aperçu (%1) :</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1541"/>
        <source>Current syntax: %1 %2</source>
        <translation>Syntaxe actuelle : %1 %2</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1542"/>
        <source>Current theme: %1 %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current syntax: %1</source>
        <translation type="vanished">Syntaxe actuelle : %1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1582"/>
        <source>Preview of &quot;%1&quot; not possible.</source>
        <translation>Impossible de donner un aperçu de &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1622"/>
        <location filename="mainwindow.cpp" line="1626"/>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="1635"/>
        <source>Choose a style include file</source>
        <translation>Choisissez un fichier de style à inclure</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1640"/>
        <source>About providing translations</source>
        <translation>À propos des traductions</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1641"/>
        <source>The GUI was developed using the Qt toolkit, and translations may be provided using the tools Qt Linguist and lrelease.
The highlight.ts file for Linguist resides in the src/gui-qt subdirectory.
The qm file generated by lrelease has to be saved in gui-files/l10n.

Please send a note to as (at) andre-simon (dot) de if you have issues during translating or if you have finished or updated a translation.</source>
        <translation>Cette interface graphique a été développée en utilisant Qt. Les traductions peuvent être fournies en utilisant les outils Qt Linguist et lrelease.
Le fichier highlight.ts pour Linguist est situé dans le sous-dossier src/gui-qt.
Le fichier .qm généré par lrelease doit être sauvegardé dans gui-files/l10n.

Veuillez m&apos;envoyer une note à l&apos;adresse « as (at) andre-simon (dot) de » si vous rencontrez des problèmes durant la traduction ou si vous avez terminé ou mis à jour une traduction.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1699"/>
        <source>Select one or more plug-ins</source>
        <translation>Sélectionner un ou plusieurs greffons</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1710"/>
        <source>Select one or more syntax or theme scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1770"/>
        <source>Choose a plug-in input file</source>
        <translation>Choisissez un fichier d&apos;entrée pour le greffon</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1828"/>
        <source>Some plug-in effects may not be visible in the preview.</source>
        <translation>Certains effets du greffon peuvent ne pas être visibles dans l&apos;aperçu.</translation>
    </message>
</context>
<context>
    <name>MainWindowClass</name>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <source>Highlight</source>
        <translation>Highlight</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="30"/>
        <source>Preview</source>
        <translation>Aperçu</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="58"/>
        <source>Output progress:</source>
        <translation>Progression :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="94"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="110"/>
        <source>Wi&amp;ndows</source>
        <translation>Fe&amp;nêtres</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="130"/>
        <source>H&amp;ighlighting options</source>
        <oldsource>Highlighting options</oldsource>
        <translation>Options de coloration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="175"/>
        <source>Files</source>
        <translation>Fichiers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="181"/>
        <source>Choose the source code files you want to convert.</source>
        <translation>Choisir les fichiers de code source que vous voulez convertir.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="187"/>
        <source>Choose input files</source>
        <translation>Choisir les fichiers d&apos;entrée</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="213"/>
        <source>List of input files.</source>
        <translation>Liste des fichiers en entrée.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="237"/>
        <source>Remove the selected input files.</source>
        <translation>Supprimer les fichiers sources sélectionnés.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="240"/>
        <location filename="mainwindow.ui" line="583"/>
        <location filename="mainwindow.ui" line="719"/>
        <source>Clear selection</source>
        <translation>Effacer la sélection</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="247"/>
        <source>Remove all input files.</source>
        <translation>Supprimer tous les fichiers d&apos;entrée.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="250"/>
        <location filename="mainwindow.ui" line="593"/>
        <location filename="mainwindow.ui" line="729"/>
        <source>Clear all</source>
        <translation>Tout effacer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="274"/>
        <source>Output destination:</source>
        <translation>Destination de la sortie :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="289"/>
        <source>Output directory</source>
        <translation>Répertoire de destination</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="308"/>
        <source>Select the output directory.</source>
        <translation>Sélectionner le répertoire de destination.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="311"/>
        <location filename="mainwindow.ui" line="670"/>
        <location filename="mainwindow.ui" line="1334"/>
        <location filename="mainwindow.ui" line="1603"/>
        <location filename="mainwindow.ui" line="1711"/>
        <location filename="mainwindow.ui" line="2006"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="330"/>
        <source>Browse output directory</source>
        <translation>Parcourir le répertoire de destination</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="346"/>
        <source>Save output in the input file directories.</source>
        <translation>Sauvegarder la sortie dans les répertoires des fichiers d&apos;entrée.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="349"/>
        <source>Write to source directories</source>
        <translation>Écrire dans les répertoires sources</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="384"/>
        <source>Start the conversion of your input files.</source>
        <translation>Démarrer la conversion de vos fichiers en entrée.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="387"/>
        <source>Convert files</source>
        <translation>Convertir les fichiers</translation>
    </message>
    <message>
        <source>Copy highlighted code of the seleted file into the clipboard.</source>
        <translation type="vanished">Copier le code coloré du fichier sélectionné dans le presse-papiers.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="400"/>
        <source>Copy file to clipboard</source>
        <translation>Copier le fichier dans le presse-papiers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="412"/>
        <source>Clipboard</source>
        <translation>Presse-papiers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="420"/>
        <source>Select syntax:</source>
        <translation>Sélectionner une syntaxe :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="427"/>
        <source>Select the correct syntax of the code snippet.</source>
        <translation>Sélectionner la syntaxe correcte de l&apos;extrait de code.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="436"/>
        <source>Paste clipboard content into the preview window.</source>
        <translation>Coller le contenu du presse-papiers dans la fenêtre de prévisualisation.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="439"/>
        <source>Paste from clipboard (%1)</source>
        <translation>Coller depuis le presse-papiers (%1)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="456"/>
        <source>Copy highlighted code into the clipboard.</source>
        <translation>Copier le code coloriée dans le presse-papiers.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="459"/>
        <source>Copy preview to clipboard (%1)</source>
        <translation>Copier l&apos;aperçu vers le presse-papiers (%1)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="482"/>
        <source>Paste code from clipboard and copy the output back in one step</source>
        <translation>Coller le code depuis le presse-papiers et recopier la sortie en une seule étape</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="488"/>
        <source>Paste, Convert and Copy</source>
        <translation>Coller, convertir et copier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="514"/>
        <source>Only output the selected lines of the preview.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="517"/>
        <source>Output selected lines only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="542"/>
        <source>Plug-ins</source>
        <translation>Greffons</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="548"/>
        <source>Choose a plug-in script.</source>
        <translation>Choisir un script de greffon.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="551"/>
        <source>Add plug-in to pool</source>
        <translation>Ajouter un greffon à la sélection</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="562"/>
        <source>List of plug-ins. Toggle checkbox to enable the scripts. The preview window may not display all plug-in effects.</source>
        <translation>Liste des greffons. Cochez pour activer les scripts. La fenêtre de prévisualisation peut ne pas afficher les effets de tous les greffons.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="580"/>
        <source>Remove the selected plug-ins.</source>
        <translation>Supprimer les greffons sélectionnés.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="590"/>
        <source>Remove all plug-ins.</source>
        <translation>Supprimer tous les greffons.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="602"/>
        <location filename="mainwindow.ui" line="738"/>
        <source>Description</source>
        <translation>Description</translation>
    </message>
    <message>
        <source>Shows decription of a selected plug-in script.</source>
        <translation type="vanished">Affiche la description d&apos;un script de greffon sélectionné.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="617"/>
        <source>Plug-In description</source>
        <translation>Description du greffon</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="645"/>
        <source>Plug-in parameter</source>
        <translation>Paramètres du greffon</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="654"/>
        <source>Optional plug-in parameter, this may be a path to a plug-in input file</source>
        <translation>Paramètres optionnels du greffon, cela peut être un chemin vers un fichier d&apos;entrée</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="667"/>
        <source>Select the plug-in input file.</source>
        <translation>Sélectionner le fichier d&apos;entrée du greffon.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="684"/>
        <source>Scripts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="690"/>
        <source>Choose a custom syntax or theme script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="693"/>
        <source>Add custom script to pool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="707"/>
        <source>You can check one file of each type (syntax and theme) to override the default behaviour. If checked, the scripts will be watched for changes.</source>
        <oldsource>You can check one file of each type (syntax and theme) to override the default behaviour.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="716"/>
        <source>Remove the selected scripts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="726"/>
        <source>Remove all scripts.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="747"/>
        <source>Script description</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="789"/>
        <source>General</source>
        <translation>Général</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="809"/>
        <source>Output for&amp;mat:</source>
        <translation>For&amp;mat de sortie :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="831"/>
        <source>Choose an output format.</source>
        <translation>Choisir un format de sortie.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="835"/>
        <source>HTML</source>
        <translation>HTML</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="840"/>
        <source>XHTML</source>
        <translation>XHTML</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="845"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="850"/>
        <source>TeX</source>
        <translation>TeX</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="855"/>
        <source>RTF</source>
        <translation>RTF</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="860"/>
        <source>ODT</source>
        <translation>ODT</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="865"/>
        <source>SVG</source>
        <translation>SVG</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="870"/>
        <source>BBCode</source>
        <translation>BBCode</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="904"/>
        <source>Add line numbers to the output.</source>
        <translation>Ajouter les numéros de ligne à la sortie.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="907"/>
        <source>Add line numbers</source>
        <translation>Ajouter les numéros de lignes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="927"/>
        <source>Set line numbering start.</source>
        <translation>Définir le début de la numérotation.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="946"/>
        <source>Select the line number width.</source>
        <translation>Sélectionner la largeur des numéros de lignes.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="967"/>
        <source>Fill leading space of line numbers with zeroes.</source>
        <translation>Remplir l&apos;espace avant les numéros de lignes par des zéros.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="970"/>
        <source>Pad with zeroes</source>
        <translation>Compléter avec des zéros</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="983"/>
        <source>Generate output without document header and footer.</source>
        <translation>Générer la sortie sans haut ni bas de page.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="986"/>
        <source>Omit header and footer</source>
        <translation>Omettre haut et bas de page</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="993"/>
        <source>Output any plug-in text injections even if document header and footer is omitted.</source>
        <translation>Conserver toutes les injections de texte des greffons, même si le haut et le bas de page sont omis.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="996"/>
        <source>Keep Plug-In injections</source>
        <translation>Garder les injections des greffons</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1003"/>
        <source>Test if input data is not binary.
Removes Unicode BOM mark.</source>
        <translation>Vérifier si les données en entrée ne sont pas binaires.
Supprime les marques Unicode BOM.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1007"/>
        <source>Validate input data</source>
        <translation>Valider les données en entrée</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1014"/>
        <source>Generate output without version information comment.</source>
        <translation>Générer la sortie sans commentaire d&apos;information sur la version.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1017"/>
        <source>Omit version info comment</source>
        <translation>Omettre les informations de version</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1026"/>
        <source>Set the output file encoding.</source>
        <translation>Définir l&apos;encodage du fichier de sortie.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1029"/>
        <source>Set encoding:</source>
        <translation>Définir l&apos;encodage :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1036"/>
        <source>Select or define the encoding.
The result has to match the input file encoding.</source>
        <translation>Sélectionner ou définir l&apos;encodage.
Le résultat doit correspondre à l&apos;encodage du fichier d&apos;entrée.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1047"/>
        <source>ISO-8859-1</source>
        <translation>ISO-8859-1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1052"/>
        <source>ISO-8859-2</source>
        <translation>ISO-8859-2</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1057"/>
        <source>ISO-8859-3</source>
        <translation>ISO-8859-3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1062"/>
        <source>ISO-8859-4</source>
        <translation>ISO-8859-4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1067"/>
        <source>ISO-8859-5</source>
        <translation>ISO-8859-5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1072"/>
        <source>ISO-8859-6</source>
        <translation>ISO-8859-6</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1077"/>
        <source>ISO-8859-7</source>
        <translation>ISO-8859-7</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1082"/>
        <source>ISO-8859-8</source>
        <translation>ISO-8859-8</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1087"/>
        <source>ISO-8859-9</source>
        <translation>ISO-8859-9</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1092"/>
        <source>ISO-8859-10</source>
        <translation>ISO-8859-10</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1097"/>
        <source>ISO-8859-11</source>
        <translation>ISO-8859-11</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1102"/>
        <source>ISO-8859-12</source>
        <translation>ISO-8859-12</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1107"/>
        <source>ISO-8859-13</source>
        <translation>ISO-8859-13</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1112"/>
        <source>ISO-8859-14</source>
        <translation>ISO-8859-14</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1117"/>
        <source>ISO-8859-15</source>
        <translation>ISO-8859-15</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1122"/>
        <source>UTF-8</source>
        <translation>UTF-8</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1146"/>
        <source>Output specific</source>
        <translation>Sortie spécifique</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1187"/>
        <source>Stylesheets</source>
        <translation>Feuilles de style</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1195"/>
        <location filename="mainwindow.ui" line="1521"/>
        <location filename="mainwindow.ui" line="1629"/>
        <location filename="mainwindow.ui" line="1924"/>
        <source>Include the style information in each output file.</source>
        <translation>Inclure les informations de style dans chaque fichier en sortie.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1198"/>
        <location filename="mainwindow.ui" line="1927"/>
        <source>Embed style (CSS)</source>
        <translation>Embarquer le style (CSS)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1211"/>
        <source>Add CSS information to each tag (do not use CSS class definitions).</source>
        <translation>Ajouter les informations CSS à chaque étiquette (ne pas utiliser les définitions de classes CSS).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1214"/>
        <source>Inline CSS</source>
        <translation>CSS en ligne</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1228"/>
        <source>CSS class prefix:</source>
        <translation>Préfixe de classe CSS :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1241"/>
        <source>Add a CSS class name prefix to avoid namespace clashes.</source>
        <translation>Ajouter un préfixe de nom de classe CSS pour éviter les conflits d&apos;espaces de noms.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1255"/>
        <location filename="mainwindow.ui" line="1533"/>
        <location filename="mainwindow.ui" line="1641"/>
        <location filename="mainwindow.ui" line="1936"/>
        <source>Stylesheet file:</source>
        <translation>Feuille de style :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1274"/>
        <location filename="mainwindow.ui" line="1657"/>
        <location filename="mainwindow.ui" line="1952"/>
        <source>Name of the referenced style file.</source>
        <translation>Nom de la feuille de style référencée.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1277"/>
        <location filename="mainwindow.ui" line="1955"/>
        <source>highlight.css</source>
        <translation>highlight.css</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1288"/>
        <location filename="mainwindow.ui" line="1563"/>
        <location filename="mainwindow.ui" line="1671"/>
        <location filename="mainwindow.ui" line="1966"/>
        <source>Stylesheet include file:</source>
        <translation>Feuille de style à inclure :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1318"/>
        <location filename="mainwindow.ui" line="1990"/>
        <source>Path of the CSS include file.</source>
        <translation>Chemin du fichier CSS à inclure.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1331"/>
        <source>Select a CSS include file.</source>
        <translation>Sélectionner un fichier CSS à inclure.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1360"/>
        <source>Misc</source>
        <translation>Divers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1372"/>
        <source>Line numbering options:</source>
        <translation>Options de numérotation des lignes :</translation>
    </message>
    <message>
        <source>Add an achor to each line.</source>
        <translation type="vanished">Ajouter une ancre à chaque ligne.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="397"/>
        <source>Copy highlighted code of the selected file into the clipboard.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="614"/>
        <source>Shows description of a selected plug-in script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="744"/>
        <source>Shows description of a selected script.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="875"/>
        <source>ANSI ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="880"/>
        <source>XTerm256 ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="885"/>
        <source>Truecolor ESC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1379"/>
        <source>Add an anchor to each line.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1382"/>
        <source>Add line anchors</source>
        <translation>Ajouter des ancres aux lignes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1389"/>
        <source>Add the filename as prefix to the anchors.</source>
        <translation>Ajouter le nom du fichier comme préfixe aux ancres.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1392"/>
        <source>Include file name in anchor</source>
        <translation>Inclure le nom du fichier dans l&apos;ancre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1399"/>
        <source>Output the lines within an ordered list.</source>
        <translation>Afficher les lignes dans une liste ordonnée.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1402"/>
        <source>Output as ordered list</source>
        <translation>Traiter comme une liste ordonnée</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1416"/>
        <source>Generate an index file with hyperlinks to all outputted files.</source>
        <translation>Générer un fichier d&apos;index avec des hyperliens vers tous les fichiers de sortie.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1419"/>
        <source>Generate index file</source>
        <translation>Générer un fichier d&apos;index</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1426"/>
        <source>Add &amp;lt;pre&amp;gt; tags to the output, if the flag &quot;No document header and footer&quot; is selected.</source>
        <translation>Ajouter des balises &amp;lt;pre&amp;gt; à la sortie, si le haut et le bas de pages sont omis.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1429"/>
        <source>Enclose in pre tags</source>
        <translation>Entourer de balises pre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1443"/>
        <source>Add HTML MIME Type when copying code to the clipboard</source>
        <translation>Ajouter le type MIME HTML lors de la copie du code dans le presse-papiers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1446"/>
        <source>Copy with MIME type</source>
        <translation>Copier avec le type MIME</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1474"/>
        <source>Replace quotes by dq sequences.</source>
        <translation>Remplacer les guillements par des séquences dq.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1477"/>
        <source>Escape quotes</source>
        <translation>Échapper les guillements</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1484"/>
        <source>Adapt output for the Babel package</source>
        <translation>Adapter la sortie au paquet Babel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1487"/>
        <source>Add Babel compatibility</source>
        <translation>Ajouter la compatibilité Babel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1494"/>
        <source>Adapt output for the Beamer package</source>
        <translation>Adapter la sortie au paquet Beamer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1497"/>
        <source>Add Beamer compatibility</source>
        <translation>Ajouter la compatibilité Beamer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1504"/>
        <source>Replace default symbols (brackets, tilde) by nice redefinitions.</source>
        <translation>Remplacer les symboles par défaut (accolades, tilde) par de belles redéfinitions.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1507"/>
        <source>Add pretty symbols</source>
        <translation>Ajouter de jolis symboles</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1524"/>
        <source>Embed style (defs)</source>
        <translation>Embarquer le style (defs)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1549"/>
        <source>Name of the referenced  style file.</source>
        <translation>Nom de la feuille de style référencée.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1552"/>
        <location filename="mainwindow.ui" line="1660"/>
        <source>highlight.sty</source>
        <translation>highlight.sty</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1587"/>
        <location filename="mainwindow.ui" line="1695"/>
        <source>Path of the style include file.</source>
        <translation>Chemin du fichier de style à inclure.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1600"/>
        <location filename="mainwindow.ui" line="1708"/>
        <location filename="mainwindow.ui" line="2003"/>
        <source>Select a style include file.</source>
        <translation>Sélectionner un fichier de style à inclure.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1632"/>
        <source>Embed style (macros)</source>
        <translation>Embarquer le style (macros)</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1737"/>
        <source>Add character stylesheets with formatting information.
You can select the stylesheets in your word processor to reformat additional text.</source>
        <translation>Ajouter des feuilles de style pour les caractères, avec des informations de formatage.
Vous pouvez sélectionner les feuilles de style dans votre logiciel de traitement de texte pour reformater du texte additionnel.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1741"/>
        <source>Add character styles</source>
        <translation>Ajouter des styles de caractère</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1748"/>
        <source>Set page color attribute to background color.</source>
        <translation>Définir l&apos;attribut de couleur de la page avec la couleur de fond.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1751"/>
        <source>Set page color</source>
        <translation>Définir la couleur de la page</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1760"/>
        <source>Page size:</source>
        <translation>Taille de la page :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1773"/>
        <source>Select a page size.</source>
        <translation>Sélectionner une taille de page.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1780"/>
        <source>A3</source>
        <translation>A3</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1785"/>
        <source>A4</source>
        <translation>A4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1790"/>
        <source>A5</source>
        <translation>A5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1795"/>
        <source>B4</source>
        <translation>B4</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1800"/>
        <source>B5</source>
        <translation>B5</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1805"/>
        <source>B6</source>
        <translation>B6</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1810"/>
        <source>Letter</source>
        <translation>Lettre</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1815"/>
        <source>Legal</source>
        <translation>Légal</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1844"/>
        <source>I&amp;mage width:</source>
        <translation>Largeur de l&apos;i&amp;mage :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1872"/>
        <source>Enter the SVG width (may contain units).</source>
        <translation>Saisir la largeur du SVG (peut contenir des unités).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1892"/>
        <source>Height:</source>
        <translation>Hauteur :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1908"/>
        <source>Enter the SVG height (may contain units).</source>
        <translation>Saisir la taille du SVG (peut contenir des unités).</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2032"/>
        <source>No options defined.</source>
        <translation>Aucune option définie.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2057"/>
        <source>Formatting</source>
        <translation>Formatage</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2086"/>
        <source>Color theme:</source>
        <translation>Thème de couleurs :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2133"/>
        <source>Select a colour theme.</source>
        <translation>Sélectionner un thème de couleurs.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2150"/>
        <source>Reformat and indent your code.
This feature is enabled for C, C++, C# and Java code.</source>
        <translation>Reformater et indenter votre code.
Cette fonctionnalité est activée pour le code C, C++, C# et Java.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2154"/>
        <source>Reformat:</source>
        <translation>Reformatage :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2173"/>
        <source>Choose a formatting scheme.</source>
        <translation>Choisir un schéma de formatage.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2187"/>
        <source>Change the keyword case.</source>
        <translation>Changer la casse.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2190"/>
        <source>Keyword case:</source>
        <translation>Casse des mots-clés :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2209"/>
        <source>Select a keyword case.</source>
        <translation>Sélectionner une casse pour les mots-clés.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2216"/>
        <source>UPPER</source>
        <translation>MAJUSCULE</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2221"/>
        <source>lower</source>
        <translation>minuscule</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2226"/>
        <source>Capitalize</source>
        <translation>Casse de phrase</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2244"/>
        <source>Tab width:</source>
        <translation>Largeur des tabulations :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2260"/>
        <source>Enter the number of spaces which replace a tab.
Set the width to 0 to keep tabs.</source>
        <translation>Saisir le nombre d&apos;espaces qui remplacent une tabulation.
Définir la largeur à 0 pour conserver les tabulations.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2275"/>
        <source>Enable line wrapping.</source>
        <translation>Activer le retour à la ligne automatique.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2278"/>
        <source>Line wrapping</source>
        <translation>Retour à la ligne</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2291"/>
        <source>Enter the maximum line length.</source>
        <translation>Saisir la taille maximale d&apos;une ligne.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2310"/>
        <source>Indent statements and function parameters after wrapping.</source>
        <translation>Indenter les déclarations et les paramètres de fonctions après le retour à ligne automatique.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2313"/>
        <source>Intelligent wrapping</source>
        <translation>Retour à la ligne intelligent</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2320"/>
        <source>Do not add line numbering to lines which were automatically wrapped.</source>
        <translation>Ne pas ajouter des numéros aux retours à la ligne automatiques.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2323"/>
        <source>Omit line numbers of wrapped lines</source>
        <translation>Omettre les numéros de ligne pour les retours à la ligne</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2334"/>
        <source>Font na&amp;me:</source>
        <translation>No&amp;m de la police :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2355"/>
        <source>Select or enter the font name. HTML supports a list of fonts, separated with comma.</source>
        <translation>Sélectionner ou saisir le nom d&apos;une police. L&apos;HTML supporte une liste de polices, séparées par des virgules.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2370"/>
        <source>Font si&amp;ze:</source>
        <translation>&amp;Taille de la police :</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2392"/>
        <source>Enter the font size.</source>
        <translation>Saisissez la taille de police.</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2407"/>
        <source>&amp;Open files</source>
        <translation>&amp;Ouvrir des fichiers</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2412"/>
        <source>&amp;Exit</source>
        <translation>Quitt&amp;er</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2417"/>
        <source>&amp;Load</source>
        <translation>&amp;Charger</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2422"/>
        <source>&amp;Save</source>
        <translation>&amp;Sauvegarder</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2427"/>
        <source>Load &amp;default project</source>
        <translation>Charger le projet par &amp;défaut</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2432"/>
        <source>&amp;Readme</source>
        <translation>Lise&amp;z-moi</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2437"/>
        <source>&amp;Tips</source>
        <translation>As&amp;tuces</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2445"/>
        <source>&amp;Changelog</source>
        <translation>Journal des &amp;changements</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2450"/>
        <source>&amp;License</source>
        <translation>&amp;Licence</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2455"/>
        <source>&amp;About Highlight</source>
        <translation>&amp;À propos de Highlight</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2460"/>
        <source>A&amp;bout translations</source>
        <translation>À propos des tra&amp;ductions</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2465"/>
        <source>&amp;Visit andre-simon.de</source>
        <translation>&amp;Visiter andre-simon.de</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2476"/>
        <source>&amp;Dock floating panels</source>
        <translation>A&amp;ccrocher les panneaux flottants</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2481"/>
        <source>&amp;Plug-Ins</source>
        <translation>&amp;Greffons</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="2486"/>
        <source>&amp;File access trace (W32)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowTextFile</name>
    <message>
        <location filename="showtextfile.ui" line="17"/>
        <source>Show text</source>
        <translation>Afficher le texte</translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="37"/>
        <source>TextLabel</source>
        <translation>ÉtiquetteTexte</translation>
    </message>
    <message>
        <location filename="showtextfile.ui" line="71"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>io_report</name>
    <message>
        <location filename="io_report.ui" line="14"/>
        <source>Error summary</source>
        <translation>Résumé des erreurs</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="24"/>
        <source>Input errors:</source>
        <translation>Erreurs d&apos;entrée :</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="34"/>
        <source>Remove files above from input file list</source>
        <translation>Supprimer ces fichiers de la liste des fichiers en entrée</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="48"/>
        <source>Output errors:</source>
        <translation>Erreurs de sortie :</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="65"/>
        <source>Reformatting not possible:</source>
        <translation>Reformatage impossible :</translation>
    </message>
    <message>
        <location filename="io_report.ui" line="82"/>
        <source>Failed syntax tests:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>syntax_chooser</name>
    <message>
        <location filename="syntax_chooser.ui" line="14"/>
        <source>Syntax selection</source>
        <translation>Sélection de la syntaxe</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="32"/>
        <source>Select correct syntax</source>
        <translation>Sélectionner la syntaxe correcte</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="50"/>
        <source>TextLabel</source>
        <translation>ÉtiquetteTexte</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="74"/>
        <source>These entries are configured in filetypes.conf.</source>
        <translation>Ces entrées sont configurés dans filetypes.conf.</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="81"/>
        <source>The selection will be remembered for this session.</source>
        <translation>La sélection sera mémorisée pour cette session.</translation>
    </message>
    <message>
        <location filename="syntax_chooser.ui" line="84"/>
        <source>Remember selection</source>
        <translation>Mémoriser la sélection</translation>
    </message>
    <message>
        <location filename="syntax_chooser.cpp" line="57"/>
        <source>The file extension %1 is assigned to multiple syntax definitions.
Select the correct one:</source>
        <translation>L&apos;extension de fichier %1 est assignée à plusieurs définitions de syntaxe.
Sélectionnez la bonne :</translation>
    </message>
</context>
</TS>
